package io.github.winx64.sobug;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.java.JavaPlugin;

import io.github.winx64.sobug.command.CommandBug;
import io.github.winx64.sobug.listener.PlayerListener;
import io.github.winx64.sobug.report.BugReport;
import io.github.winx64.sobug.report.BugResolution;
import io.github.winx64.sobug.report.ReportManager;

public final class SoBug extends JavaPlugin {

	private final Logger logger;

	private final ReportManager reportManager;

	private boolean initialized;

	public SoBug() {
		this.logger = getLogger();

		this.reportManager = new ReportManager(this);

		this.initialized = false;
	}

	@Override
	public void onEnable() {
		ConfigurationSerialization.registerClass(BugReport.class);
		ConfigurationSerialization.registerClass(BugResolution.class);

		if (!reportManager.loadData()) {
			stop("Report load error");
			return;
		}
		log("Reports loaded with success!");

		if (!registerCommands()) {
			stop("Command registration error");
			return;
		}

		Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);
		this.initialized = true;
	}

	@Override
	public void onDisable() {
		if (initialized) {
			if (reportManager.saveData()) {
				log("Reports saved with success!");
			}
		}
	}

	private boolean registerCommands() {
		try {
			getCommand("bug").setExecutor(new CommandBug(this));

			return true;
		} catch (Exception e) {
			log(Level.SEVERE, e, "An error occurred while trying to register the commands! Details below:");
			return false;
		}
	}

	private void stop(String reason) {
		log(Level.SEVERE, "The plugin will now be dissabled! Reason: %s", reason);
		Bukkit.getPluginManager().disablePlugin(this);
	}

	public ReportManager getReportManager() {
		return reportManager;
	}

	public void log(String format, Object... args) {
		log(Level.INFO, null, String.format(format, args));
	}

	public void log(String message) {
		log(Level.INFO, null, message);
	}

	public void log(Level level, String format, Object... args) {
		log(level, null, String.format(format, args));
	}

	public void log(Level level, Exception e, String format, Object... args) {
		log(level, e, String.format(format, args));
	}

	public void log(Level level, String message) {
		log(level, null, message);
	}

	public void log(Level level, Exception e, String message) {
		logger.log(level, message, e);
	}
}
