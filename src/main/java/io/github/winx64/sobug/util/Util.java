package io.github.winx64.sobug.util;

import java.util.Arrays;

public final class Util {

	private Util() {}

	public static String stringJoin(String[] args, String joint) {
		return stringJoin(Arrays.asList(args), joint);
	}

	public static String stringJoin(Iterable<String> args, String joint) {
		StringBuilder builder = new StringBuilder();
		for (String arg : args) {
			builder.append(joint).append(arg);
		}
		return builder.length() < 1 ? "" : builder.substring(joint.length());
	}

	public static Integer tryParseInt(String value) {
		try {
			return Integer.valueOf(value);
		} catch (Exception ignored) {
			return null;
		}
	}
}
