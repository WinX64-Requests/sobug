package io.github.winx64.sobug.report;

import static io.github.winx64.sobug.report.ReportManager.DATE;
import static io.github.winx64.sobug.report.ReportManager.ID;
import static io.github.winx64.sobug.report.ReportManager.REASON;
import static io.github.winx64.sobug.report.ReportManager.UNIQUE_ID;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Player;

@SerializableAs("Report")
public final class BugReport implements ConfigurationSerializable {

	private final int id;
	private final UUID reporterUniqueId;
	private final long reportDate;
	private final String reportReason;

	private BugResolution resolution;

	public BugReport(int id, UUID reporterUniqueId, long reportDate, String reportReason) {
		this.id = id;
		this.reporterUniqueId = reporterUniqueId;
		this.reportDate = reportDate;
		this.reportReason = reportReason;

		this.resolution = null;
	}

	public BugReport(Map<String, Object> map) {
		Object idObj = map.get(ID);
		if (idObj == null || !(idObj instanceof Integer)) {
			throw new IllegalArgumentException(ID + " must be an Integer");
		}

		Object reporterUniqueIdObj = map.get(UNIQUE_ID);
		if (reporterUniqueIdObj == null || !(reporterUniqueIdObj instanceof String)) {
			throw new IllegalArgumentException(UNIQUE_ID + " must be a String");
		}

		Object reportDateObj = map.get(DATE);
		if (reportDateObj == null || !(reportDateObj instanceof Long)) {
			throw new IllegalArgumentException(DATE + " must be a Long");
		}

		Object reportReasonObj = map.get(REASON);
		if (reportReasonObj == null || !(reportReasonObj instanceof String)) {
			throw new IllegalArgumentException(REASON + " must be a String");
		}

		this.id = (Integer) idObj;
		this.reporterUniqueId = UUID.fromString((String) reporterUniqueIdObj);
		this.reportDate = (Long) reportDateObj;
		this.reportReason = (String) reportReasonObj;
	}

	public Map<String, Object> serialize() {
		Map<String, Object> map = new LinkedHashMap<String, Object>();

		map.put(ID, id);
		map.put(UNIQUE_ID, reporterUniqueId.toString());
		map.put(DATE, reportDate);
		map.put(REASON, reportReason);

		return map;
	}

	public final int getId() {
		return id;
	}

	public final UUID getReporterUniqueId() {
		return reporterUniqueId;
	}

	public final long getReportDate() {
		return reportDate;
	}

	public final String getReportReason() {
		return reportReason;
	}

	public BugResolution getResolution() {
		return resolution;
	}

	public void setResolution(BugResolution resolution) {
		this.resolution = resolution;
	}

	public void resolve(Player resolver, String reason) {
		this.resolution = new BugResolution(id, resolver.getUniqueId(), System.currentTimeMillis(), reason);
	}

	@Override
	public final boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (!(o instanceof BugReport)) {
			return false;
		}

		BugReport report = (BugReport) o;
		return id == report.id;
	}

	@Override
	public final int hashCode() {
		return id;
	}
}
