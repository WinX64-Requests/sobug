package io.github.winx64.sobug.report;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.UUID;
import java.util.logging.Level;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;

import io.github.winx64.sobug.SoBug;

public final class ReportManager {

	private static final String REPORTS_FILE_NAME = "reports.yml";
	private static final String RESOLUTIONS_FILE_NAME = "resolutions.yml";
	private static final String NAME_CACHE_FILE_NAME = "name-cache.yml";

	static final String ID = "id";
	static final String UNIQUE_ID = "unique-id";
	static final String DATE = "date";
	static final String REASON = "reason";

	private final SoBug plugin;

	private final File reportsFile;
	private final File resolutionsFile;
	private final File nameCacheFile;

	private int currentReportId;
	private final Map<Integer, BugReport> bugReports;
	private final Multimap<UUID, BugReport> bugReportsByUniqueId;
	private final BiMap<UUID, String> nameCache;

	public ReportManager(SoBug plugin) {
		this.plugin = plugin;

		this.reportsFile = new File(plugin.getDataFolder(), REPORTS_FILE_NAME);
		this.resolutionsFile = new File(plugin.getDataFolder(), RESOLUTIONS_FILE_NAME);
		this.nameCacheFile = new File(plugin.getDataFolder(), NAME_CACHE_FILE_NAME);

		this.currentReportId = 0;
		this.bugReports = new TreeMap<Integer, BugReport>();
		this.bugReportsByUniqueId = MultimapBuilder.hashKeys().treeSetValues(new Comparator<BugReport>() {
			public int compare(BugReport one, BugReport two) {
				return one.getId() - two.getId();
			}
		}).build();
		this.nameCache = HashBiMap.create();
	}

	public boolean loadData() {
		try {
			if (!reportsFile.exists()) {
				plugin.saveResource(REPORTS_FILE_NAME, true);
			}
			if (!resolutionsFile.exists()) {
				plugin.saveResource(RESOLUTIONS_FILE_NAME, true);
			}
			FileConfiguration reports = YamlConfiguration.loadConfiguration(reportsFile);
			FileConfiguration resolutions = YamlConfiguration.loadConfiguration(resolutionsFile);
			FileConfiguration nameCache = YamlConfiguration.loadConfiguration(nameCacheFile);

			loadReports(reports.getList("bug-reports"));
			loadResolutions(resolutions.getList("bug-resolutions"));
			loadNameCache(nameCache);

			return true;
		} catch (Exception e) {
			plugin.log(Level.SEVERE, e, "An error occurred while trying to load the bug reports! Details below:");
			return false;
		}
	}

	public boolean saveData() {
		try {
			List<BugReport> reportsList = new ArrayList<BugReport>(this.bugReports.values());
			List<BugResolution> resolutionsList = new ArrayList<BugResolution>();
			for (BugReport report : bugReports.values()) {
				if (report.getResolution() != null) {
					resolutionsList.add(report.getResolution());
				}
			}

			FileConfiguration reports = new YamlConfiguration();
			FileConfiguration resolutions = new YamlConfiguration();
			FileConfiguration nameCache = new YamlConfiguration();

			reports.set("bug-reports", reportsList);
			resolutions.set("bug-resolutions", resolutionsList);
			for (Entry<UUID, String> entry : this.nameCache.entrySet()) {
				nameCache.set(entry.getKey().toString(), entry.getValue());
			}

			reports.save(reportsFile);
			resolutions.save(resolutionsFile);
			nameCache.save(nameCacheFile);
			return true;
		} catch (Exception e) {
			plugin.log(Level.SEVERE, e, "An error occurred while trying to save the bug reports! Details below:");
			return false;
		}
	}

	public Collection<BugReport> getBugReports() {
		return Collections.unmodifiableCollection(bugReports.values());
	}

	public Collection<BugReport> getBugReports(Player player) {
		return getBugReports(player.getUniqueId());
	}

	public Collection<BugReport> getBugReports(UUID uniqueId) {
		return Collections.unmodifiableCollection(bugReportsByUniqueId.get(uniqueId));
	}

	public Collection<BugReport> getBugReports(String name) {
		UUID cachedUniqueId = getCachedUniqueId(name);
		if (cachedUniqueId == null) {
			return null;
		}
		return Collections.unmodifiableCollection(bugReportsByUniqueId.get(cachedUniqueId));
	}

	public String getCachedName(UUID uniqueId) {
		return nameCache.get(uniqueId);
	}

	public UUID getCachedUniqueId(String name) {
		return nameCache.inverse().get(name.toLowerCase());
	}

	public void updateNameCache(Player player) {
		this.nameCache.forcePut(player.getUniqueId(), player.getName().toLowerCase());
	}

	public BugReport registerReport(Player reporter, String reason) {
		BugReport report = new BugReport(++currentReportId, reporter.getUniqueId(), System.currentTimeMillis(), reason);
		this.bugReports.put(report.getId(), report);
		this.bugReportsByUniqueId.put(report.getReporterUniqueId(), report);

		return report;
	}

	public BugReport getBugReport(int reportId) {
		return bugReports.get(reportId);
	}

	private void loadReports(List<?> reportsList) {
		for (Object reportObj : reportsList) {
			if (!(reportObj instanceof BugReport)) {
				plugin.log(Level.WARNING, "Invalid report of type '%s' found in the configuration. Skipping!",
						reportObj.getClass().getSimpleName());
				continue;
			}

			BugReport report = (BugReport) reportObj;
			this.bugReports.put(report.getId(), report);
			this.bugReportsByUniqueId.put(report.getReporterUniqueId(), report);

			if (report.getId() > currentReportId) {
				currentReportId = report.getId();
			}
		}
	}

	private void loadResolutions(List<?> resolutionsList) {
		for (Object resolutionObj : resolutionsList) {
			if (!(resolutionObj instanceof BugResolution)) {
				plugin.log(Level.WARNING, "Invalid resolution of type '%s' found in the configuration. Skipping!",
						resolutionObj.getClass().getSimpleName());
				continue;
			}

			BugResolution resolution = (BugResolution) resolutionObj;
			BugReport report = this.bugReports.get(resolution.getId());
			if (report != null) {
				report.setResolution(resolution);
			}
		}
	}

	private void loadNameCache(ConfigurationSection nameSection) {
		for (String uniqueIdKey : nameSection.getKeys(false)) {
			UUID uniqueId = UUID.fromString(uniqueIdKey);
			String name = nameSection.getString(uniqueIdKey);

			this.nameCache.forcePut(uniqueId, name.toLowerCase());
		}
	}
}
