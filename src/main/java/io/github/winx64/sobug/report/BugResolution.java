package io.github.winx64.sobug.report;

import static io.github.winx64.sobug.report.ReportManager.DATE;
import static io.github.winx64.sobug.report.ReportManager.ID;
import static io.github.winx64.sobug.report.ReportManager.REASON;
import static io.github.winx64.sobug.report.ReportManager.UNIQUE_ID;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;

@SerializableAs("Resolution")
public final class BugResolution implements ConfigurationSerializable {

	private final int id;
	private final UUID resolverUniqueId;
	private final long resolveDate;
	private final String resolveReason;

	public BugResolution(int id, UUID resolverUniqueId, long resolveDate, String resolveReason) {
		this.id = id;
		this.resolverUniqueId = resolverUniqueId;
		this.resolveDate = resolveDate;
		this.resolveReason = resolveReason;
	}

	public BugResolution(Map<String, Object> map) {
		Object idObj = map.get(ID);
		if (idObj == null || !(idObj instanceof Integer)) {
			throw new IllegalArgumentException(ID + " must be an Integer");
		}

		Object resolverUniqueIdObj = map.get(UNIQUE_ID);
		if (resolverUniqueIdObj == null || !(resolverUniqueIdObj instanceof String)) {
			throw new IllegalArgumentException(UNIQUE_ID + " must be a String");
		}

		Object resolveDateObj = map.get(DATE);
		if (resolveDateObj == null || (!(resolveDateObj instanceof Integer) && !(resolveDateObj instanceof Long))) {
			throw new IllegalArgumentException(DATE + " must be an Integer or Long");
		}

		Object resolveReasonObj = map.get(REASON);
		if (resolveReasonObj == null || !(resolveReasonObj instanceof String)) {
			throw new IllegalArgumentException(REASON + " must be a String");
		}

		this.id = (Integer) idObj;
		this.resolverUniqueId = UUID.fromString((String) resolverUniqueIdObj);
		this.resolveDate = (Long) resolveDateObj;
		this.resolveReason = (String) resolveReasonObj;
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> map = new LinkedHashMap<String, Object>();

		map.put(ID, id);
		map.put(UNIQUE_ID, resolverUniqueId.toString());
		map.put(DATE, resolveDate);
		map.put(REASON, resolveReason);

		return map;
	}

	public static BugResolution deserialize(Map<String, Object> map) {
		return new BugResolution(map);
	}

	public int getId() {
		return id;
	}

	public UUID getResolverUniqueId() {
		return resolverUniqueId;
	}

	public long getResolveDate() {
		return resolveDate;
	}

	public String getResolveReason() {
		return resolveReason;
	}

	@Override
	public final boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (!(o instanceof BugResolution)) {
			return false;
		}

		BugResolution resolution = (BugResolution) o;
		return id == resolution.id;
	}

	@Override
	public final int hashCode() {
		return id;
	}
}
