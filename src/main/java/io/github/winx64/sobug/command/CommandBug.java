package io.github.winx64.sobug.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import io.github.winx64.sobug.SoBug;
import io.github.winx64.sobug.report.BugReport;
import io.github.winx64.sobug.report.BugResolution;
import io.github.winx64.sobug.util.Util;
import net.md_5.bungee.api.ChatColor;

public final class CommandBug implements TabExecutor {

	private static final List<String> EMPTY = Collections.emptyList();

	private static final List<String> OPTIONS = Arrays.asList("report", "info", "list", "resolve");

	private final SoBug plugin;

	public CommandBug(SoBug plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "This command is only available for players!");
			return true;
		}
		if (args.length < 1) {
			sender.sendMessage(String.format(ChatColor.RED + "Syntax: /%s <option> [...]", alias));
			sender.sendMessage(
					ChatColor.GRAY + "Available options: " + ChatColor.WHITE + Util.stringJoin(OPTIONS, ", "));
			return true;
		}

		Player player = (Player) sender;
		switch (args[0].toLowerCase()) {
			case "report":
				subCommandReport(player, alias, args);
				return true;

			case "info":
				subCommandInfo(player, alias, args);
				return true;

			case "list":
				subCommandList(player, alias, args);
				return true;

			case "resolve":
				subCommandResolve(player, alias, args);
				return true;

			default:
				sender.sendMessage(ChatColor.RED + "Invalid option!");
				sender.sendMessage(
						ChatColor.GRAY + "Available options: " + ChatColor.WHITE + Util.stringJoin(OPTIONS, ", "));
				return true;
		}
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		if (!(sender instanceof Player)) {
			return EMPTY;
		}
		List<String> matches = new ArrayList<String>();
		String match = args[args.length - 1].toLowerCase();
		if (args.length == 1) {
			for (String option : OPTIONS) {
				if (option.startsWith(match)) {
					matches.add(option);
				}
			}
		}
		return matches;
	}

	private void subCommandReport(Player player, String alias, String[] args) {
		if (args.length < 2) {
			player.sendMessage(String.format(ChatColor.RED + "Syntax: /%s report <reason>", alias));
			return;
		}

		String reason = Util.stringJoin(Arrays.copyOfRange(args, 1, args.length), " ");
		BugReport report = plugin.getReportManager().registerReport(player, reason);

		player.sendMessage(ChatColor.GREEN + "Bug reported with success!");
		player.sendMessage(ChatColor.GRAY + "The id of the report is " + ChatColor.YELLOW + "#" + report.getId());
	}

	private void subCommandInfo(Player player, String alias, String[] args) {
		if (args.length < 2) {
			player.sendMessage(String.format(ChatColor.RED + "Syntax: /%s info <id>", alias));
			return;
		}

		Integer id = Util.tryParseInt(args[1]);
		if (id == null) {
			player.sendMessage(String.format(ChatColor.RED + "The value '%s' is not a valid integer!", args[1]));
			return;
		}

		BugReport report = plugin.getReportManager().getBugReport(id);
		if (report == null) {
			player.sendMessage(String.format(ChatColor.RED + "There're no reports by the id of %d!", id));
			return;
		}

		BugResolution resolution = report.getResolution();
		player.sendMessage(ChatColor.YELLOW + "===== #" + id + " =====");
		player.sendMessage(ChatColor.GRAY + "Reported by: " + ChatColor.AQUA + report.getReporterUniqueId());
		player.sendMessage(ChatColor.GRAY + "Reason: " + ChatColor.RESET + report.getReportReason());
		if (resolution != null) {
			player.sendMessage(ChatColor.GRAY + "Status: " + ChatColor.RED + "RESOLVED");
			player.sendMessage(ChatColor.YELLOW + "----------");
			player.sendMessage(ChatColor.GRAY + "Resolved by: " + ChatColor.AQUA + resolution.getResolverUniqueId());
			player.sendMessage(ChatColor.GRAY + "Reason: " + ChatColor.RESET + resolution.getResolveReason());
		} else {
			player.sendMessage(ChatColor.GRAY + "Status: " + ChatColor.GREEN + "OPEN");
		}
	}

	private void subCommandList(Player player, String alias, String[] args) {
		Collection<BugReport> reports;
		if (args.length < 2) {
			reports = plugin.getReportManager().getBugReports();
		} else {
			String name = args[1];
			reports = plugin.getReportManager().getBugReports(name);

			if (reports == null) {
				player.sendMessage(String.format(ChatColor.RED + "No player by the name of '%s' found!", name));
				return;
			}
		}

		String reportList = reportJoin(reports, ChatColor.RESET + ", ");

		if (args.length < 2) {
			player.sendMessage(ChatColor.YELLOW + "Reports: " + reportList);
		} else {
			player.sendMessage(String.format(ChatColor.YELLOW + "Reports sent by %s: " + reportList, args[1]));
		}
		player.sendMessage(String.format(
				ChatColor.GRAY + "For more information on a report, use " + ChatColor.AQUA + "/%s info <id>", alias));
		if (args.length < 2) {
			player.sendMessage(String.format(ChatColor.GRAY + "For a list of bugs reported by a specific player, use "
					+ ChatColor.AQUA + "/%s list <name>", alias));
		}
	}

	private void subCommandResolve(Player player, String alias, String[] args) {
		if (args.length < 3) {
			player.sendMessage(String.format(ChatColor.RED + "Syntax: /%s resolve <id> <reason>", alias));
			return;
		}

		Integer id = Util.tryParseInt(args[1]);
		if (id == null) {
			player.sendMessage(String.format(ChatColor.RED + "The value '%s' is not a valid integer!", args[1]));
			return;
		}

		BugReport report = plugin.getReportManager().getBugReport(id);
		if (report == null) {
			player.sendMessage(String.format(ChatColor.RED + "There're no reports by the id of %d!", id));
			return;
		}

		if (report.getResolution() != null) {
			player.sendMessage(ChatColor.RED + "This report is already solved!");
			return;
		}

		String reason = Util.stringJoin(Arrays.copyOfRange(args, 2, args.length), " ");
		report.resolve(player, reason);

		player.sendMessage(ChatColor.GREEN + "Report resolved with success!");

		Player reporter = Bukkit.getPlayer(report.getReporterUniqueId());
		if (reporter != null) {
			player.sendMessage(String.format(ChatColor.GREEN + "Your report " + ChatColor.YELLOW + "#%d "
					+ ChatColor.GREEN + "has been resolved!", id));
		}
	}

	private static String reportJoin(Iterable<BugReport> reports, String joint) {
		StringBuilder builder = new StringBuilder();
		for (BugReport report : reports) {
			builder.append(joint).append(report.getResolution() == null ? ChatColor.GREEN : ChatColor.RED)
					.append(report.getId());
		}
		return builder.length() < 1 ? "" : builder.substring(joint.length());
	}
}
