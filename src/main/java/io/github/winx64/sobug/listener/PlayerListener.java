package io.github.winx64.sobug.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import io.github.winx64.sobug.SoBug;

public final class PlayerListener implements Listener {

	private final SoBug plugin;

	public PlayerListener(SoBug plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		plugin.getReportManager().updateNameCache(player);
	}
}
